package com.xgc.rtspconverterflvspringbootstarter.controller;

import com.xgc.rtspconverterflvspringbootstarter.service.IFLVService;
import com.xgc.rtspconverterflvspringbootstarter.service.IOpenFLVService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * FLV流转换
 * 
 * @author gc.x
 */
@Api(tags = "flv")
@RestController
public class FLVController {

	@Autowired
	private IFLVService service;
	@Autowired
	private IOpenFLVService openFLVService;

	@GetMapping(value = "/flv/hls/stream_{channel}.flv")
	public void open4(@PathVariable(value = "channel") Integer channel,HttpServletResponse response,
					  HttpServletRequest request) {
		String url = openFLVService.getUrl(channel);
		if(!StringUtils.isEmpty(url)){
			service.open(url, response, request);
		}
	}
}
